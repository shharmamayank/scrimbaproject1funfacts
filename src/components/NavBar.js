import React from 'react'
import "./NavBar.css"
export default function NavBar() {
    return (
        <>
            <div className='main-conatiner'>
                <div className='nav-container'>
                    <img className='nav-img' src="./logo192.png" alt="" />
                    <h1 className='navbar-header'>ReactFacts</h1>
                </div>
                <div className='nav-container-2'>
                    <h3 className='navbar-header-2'>React Course - Project 1</h3>
                </div>
            </div>

        </>
    )
}
